import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Register () {
	const { user } = useContext(UserContext);
	const [firstName, setFirstName] = useState('');
	const [mName, setMName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);
	const [isMatched, setIsMatched] = useState(false);
	const [isBlank, setIsBlank] = useState(true);

	useEffect(() => {
		if (password1 !== '' && password1 === password2 && firstName !== '' && mName !== '' && lastName !== '' && email !== '') {
			setIsActive(true);
			setIsBlank(false);
			setIsMatched(true);
		}
		else if (password1 !== '' && password1 === password2) {
			setIsBlank(false);
			setIsMatched(true);
			if (firstName === '' && mName === '' && lastName === '' && email === '') {
				setIsActive(false);
			} else {
				setIsActive(true);
			}
		}
		else {
			setIsActive(false);
			setIsMatched(false);
		}	

	},[password1, password2, firstName, mName, lastName, email]);

	const registerUser = async (eventSubmit) => {
		eventSubmit.preventDefault();

		const isRegistered = await fetch('https://sheltered-refuge-40596.herokuapp.com/users/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
			    firstName: firstName,
			    middleName: mName,
			    lastName: lastName,
			    email: email,
			    password: password1
			})
		}).then(response => response.json()).then(dataJSON => {
			console.log(dataJSON);
			if (dataJSON.email) {
				return true;
			} else {
				return false;
			}
		})
		console.log(isRegistered);
		
		if (isRegistered) {
			setFirstName('');
			setMName('');
			setLastName('');
			setEmail('');
			setPassword1('');
			setPassword2('');

			await Swal.fire({
					icon: "success",
					title: 'Registration Successful!',
					text: 'Thanks for signing up. You can now login.'
				})
			window.location.href = "/login";
		} else {
			await Swal.fire({
				icon: "error",
				title: 'Something Went Wrong',
				text: 'Try Again Later!'
			})
		}
	};

	return (
		user.id
		?
			<Navigate to="/" replace={true}/>
		:
		<>
			<div className="jumbotronForm">
				<div className="outer-wrapper p-2">
					<div className="form-wrapper col-md-6 col-lg-4 text-white">
						<Container>
							<img src="sign-in.png" alt="" className="my-2 centerIcon" />
							<h3 className="text-center">Registration</h3>
							
							<Form onSubmit={e => registerUser(e)}>
								<Form.Group>
									<Form.Label>First Name:</Form.Label>
									<Form.Control type="text" placeholder="Juan"
									required
									value={firstName}
									onChange={event => {setFirstName(event.target.value)}}
									/>
								</Form.Group>
								<Form.Group>
									<Form.Label>Middle Name/Initial:</Form.Label>
									<Form.Control type="text" placeholder="Martinez"
									required
									value={mName}
									onChange={event => {setMName(event.target.value)}}
									/>
								</Form.Group>
								<Form.Group>
									<Form.Label>Last Name:</Form.Label>
									<Form.Control type="text" placeholder="Dela Cruz"
									required
									value={lastName}
									onChange={event => {setLastName(event.target.value)}}
									/>
								</Form.Group>
								<Form.Group>
									<Form.Label>Email:</Form.Label>
									<Form.Control type="email" placeholder="example@email.com"
									required
									value={email}
									onChange={event => {setEmail(event.target.value)}}
									/>
								</Form.Group>
								<Form.Group>
									<Form.Label>Password:</Form.Label>
									<Form.Control type="password" placeholder="Enter your password"
									required
									value={password1}
									onChange={event => {setPassword1(event.target.value)}}
									/>
								</Form.Group>
								<Form.Group>
									<Form.Label>Confirm Password:</Form.Label>
									<Form.Control type="password" placeholder="Re-enter your password"
									required
									value={password2}
									onChange={event => {setPassword2(event.target.value)}}
									/>
									{	
										isBlank ?
											<span className="text-success"></span>
										: (
											isMatched ? 
												<span className="text-success">Passwords matched!</span>
											:
												<span className="text-danger">Passwords do not match!
												</span> 
										)	  	
									}
								</Form.Group>
								<Form.Group className="my-2">
								{
									isActive ?
										<Button className="btn-primary btn-block mt-4" type="submit">Register
										</Button>
										:
										<Button className="btn-primary btn-block mt-4" disabled>Register
										</Button>
								}
								<p className="mt-2 text-center">Already registered? <a href="/login">Login here!</a></p>
								</Form.Group>
							</Form>
						</Container>
					</div>
				</div>
			</div>
		</>	
	);
};