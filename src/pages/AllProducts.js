import AllProductCard from './../components/AllProductCard';
import { Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function AllProducts () {
	const [productCollection, setProductCollection] = useState([]);
	const {user} = useContext(UserContext);
	let token = localStorage.getItem('accessToken')

	useEffect(() => {
		fetch('https://sheltered-refuge-40596.herokuapp.com/products/all', {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		}).then(res => res.json())
		.then(convertedData => {
			setProductCollection(convertedData.map(product => {
				return(
					<AllProductCard key={product.id}productProp={product} />
				)
			}))
		})
	},[token])

	return(
		!user.isAdmin
		?
			<Navigate to="/" replace={true}/>
		:
		<>
			<h1 className="text-center mt-4">ALL RECIPES</h1>
			<Container>				
					{productCollection}
			</Container>
		</>
	);
};