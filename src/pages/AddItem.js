import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useState, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function AddItem () {
	const {user} = useContext(UserContext);
	const [productName, setProductName] = useState('');
	const [productDescription, setProductDescription] = useState('');
	const [productPrice, setProductPrice] = useState('');

	let token = localStorage.getItem('accessToken');

	const addProduct = async (eventSubmit) => {
		eventSubmit.preventDefault();

		const isProductAdded = await fetch('https://sheltered-refuge-40596.herokuapp.com/products/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
			    name: productName,
			    description: productDescription,
			    price: productPrice
			})
		}).then(response => response.json()).then(dataJSON => {
			console.log(dataJSON);
			if (dataJSON) {
				return true;
			} else {
				return false;
			}
		})
		console.log(isProductAdded);
		
		if (isProductAdded) {
			setProductName('');
			setProductDescription('');
			setProductPrice('');

			await Swal.fire({
					icon: "success",
						title: 'Add Item Successful!',
						text: 'New product has been successfully created and should now be available in the Products Catalog page.'
					})
			window.location.href = "/products";
		} else {
			await Swal.fire({
				icon: "error",
				title: 'Something Went Wrong',
				text: 'Try Again Later!'
			})
		}
	};

	return (
		!user.isAdmin
		?
			<Navigate to="/" replace={true}/>
		:
		<>
			<div className="jumbotronForm">
				<div className="outer-wrapper p-2">
					<div className="form-wrapper col-md-6 col-lg-4">
						<Container>
							<img src="add.png" alt="" className="my-2 centerIcon" />
							<h3 className="text-center">Add Item</h3>
							<Form onSubmit={e => addProduct(e)}>
								<Form.Group>
									<Form.Label>Product Name:</Form.Label>
									<Form.Control type="text"
									required
									value={productName}
									onChange={event => {setProductName(event.target.value)}}
									/>
								</Form.Group>
								<Form.Group>
									<Form.Label>Description:</Form.Label>
									<Form.Control type="text"
									required
									value={productDescription}
									onChange={event => {setProductDescription(event.target.value)}}
									/>
								</Form.Group>
								<Form.Group>
									<Form.Label>Price:</Form.Label>
									<Form.Control type="number"
									required
									value={productPrice}
									onChange={event => {setProductPrice(event.target.value)}}
									/>
								</Form.Group>
								<Form.Group>
								<Button className="btn-primary btn-block my-4" type="submit">Submit</Button>
								</Form.Group>
							</Form>
						</Container>
					</div>
				</div>
			</div>
		</>
	);
};