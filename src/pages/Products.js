import ProductCard from './../components/ProductCard';
import { Container } from 'react-bootstrap';
import { useState, useEffect } from 'react';


export default function Products () {
	const [productCollection, setProductCollection] = useState([]);

	useEffect(() => {
		fetch('https://sheltered-refuge-40596.herokuapp.com/products/').then(res => res.json())
		.then(convertedData => {
			setProductCollection(convertedData.map(product => {
				return(
					<ProductCard key={product.id}productProp={product} />
				)
			}))
		})
	},[])

	return(
		<>
			<h1 className="text-center mt-4">RECIPES</h1>
			<Container>				
					{productCollection}
			</Container>
		</>
	);
};