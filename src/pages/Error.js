export default function ErrorPage () {
	return(
		<div className="text-center mt-4">
			<h1 className="my-3">404 Page Not Found</h1>
			<h5>The page you are looking for does not exist.</h5>
		</div>
	);
};