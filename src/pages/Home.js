import Banner from './../components/Banner';

const details = {
	title: 'It\'s Coffee Time!',
	content: 'It\'s such a beautiful time of the day. Chill and take a sip of a delicious iced-coffee.'
}

export default function Home () {
	return (
		<div className="jumbotron">
			<Banner bannerData={details}/>	
		</div>
  );
}