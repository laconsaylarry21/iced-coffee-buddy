import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function Login () {
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);
	const [isEmailValid, setIsEmailValid] = useState(false);
	let addressSign = email.search('@');
	let dns = email.search('.com');


	useEffect(() => {
		if (dns !== -1 && addressSign !== -1) {
			setIsEmailValid(true);
			if (password !== '') {
				setIsActive(true);
			} else {
				setIsActive(false);
			}
		} else {
			setIsEmailValid(false);
			setIsActive(false);
		}
	},[email, password, addressSign, dns])

	const loginUser = (eventSubmit) => {
		eventSubmit.preventDefault();
		fetch('https://sheltered-refuge-40596.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
			    email: email,
			    password: password
			})
		}).then(res => res.json()).then(dataJSON => {
			let token = dataJSON.access;
			// console.log(token);
			if (typeof token !== 'undefined') {
				localStorage.setItem('accessToken',token);

				fetch('https://sheltered-refuge-40596.herokuapp.com/users/my-profile', {
				  headers: {
				      Authorization: `Bearer ${token}`
				  }
				})
				.then(res => res.json())
				.then(convertedData => {
				  // console.log(convertedData)
				  if (typeof convertedData._id !== "undefined") {
				    setUser({
				      id: convertedData._id,
				      isAdmin: convertedData.isAdmin
				    });
				    Swal.fire({
				    	icon: "success",
				    	title: 'Login Successful!',
				    	text: 'Welcome to your account.'
				    })
				  } else {
				    setUser({
				      id: null,
				      isAdmin: null
				    });
				  }
				});

				
			} else {
				Swal.fire({
					icon: "error",
					title: 'Login Failed!',
					text: 'Invalid credentials. Contact Admin if problem persist.'
				})
			}
		})
	};

	return (
		user.id
		?
			<Navigate to="/" replace={true}/>
		:
		<>
			<div  className="jumbotronForm">
				<div className="outer-wrapper p-2">
					<div className="form-wrapper col-md-6 col-lg-4 text-white">
						<Container>
							<img src="sign-in.png" alt="" className="my-2 centerIcon" />
							<h3 className="text-center">Sign In</h3>
							<Form onSubmit={e => loginUser(e)}>
								<Form.Group>
									<Form.Label>Email:</Form.Label>
									<Form.Control type="email" placeholder="example@email.com"
									required
									value={email}
									onChange={event => {setEmail(event.target.value)}}
									/>
									{
										isEmailValid ? 
											<span className="text-success">Email is valid!</span>
										:
											<span className="text-danger">Email is invalid!
											</span> 	  	
									}
								</Form.Group>
								<Form.Group>
									<Form.Label>Password:</Form.Label>
									<Form.Control type="password" placeholder="Enter your password"
									required
									value={password}
									onChange={event => {setPassword(event.target.value)}}
									/>
								</Form.Group>
								<Form.Group>
								{
									isActive ?
										<Button className="btn-primary btn-block mt-4" type="submit">Login
										</Button>
										:
										<Button className="btn-primary btn-block mt-4" disabled>Login
										</Button>
								}
								<p className="mt-2 text-center">Don't have an account yet? <a href="/register">Sign Up!</a></p>
								</Form.Group>
							</Form>
						</Container>
					</div>
				</div>
			</div>
		</>
	);
};