
import { Col, Card, Button, Container } from 'react-bootstrap';
import {Link, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

export default function ProductView () {
	const {user} = useContext(UserContext);
	// console.log(user.isAdmin);
	const [productInfo, setProductInfo] = useState({
		name: null,
		description: null,
		price: null
	});

	/*console.log(useParams());*/
	const {id} = useParams()
	/*console.log(id);*/

	useEffect(() => {
		fetch(`https://sheltered-refuge-40596.herokuapp.com/products/${id}`).then(res => res.json()).then(convertedData => {
			// console.log(convertedData);

			setProductInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		});
	},[id])

	const buy = () => {
		return(
			user.isAdmin ?
				Swal.fire({
					icon: "error",
					title: 'Sorry!',
					text: 'You\'re not allowed to make purchases with this account.\nHowever, you may want to login as a regular user so you can place an order.\nThank you!'
				})
			:
				Swal.fire({
					icon: "success",
					title: 'Thank you!',
					text: 'You\'re almost done. You\'ll now be directed to the checkout page to confirm order details.'
				})
		);
	};

	return (
		<div>
			<div><h1 className="productViewText mt-4 text-center">{productInfo.name}</h1></div>
			<Container className="d-md-flex">
				<Col xs={12} md={6}>
					<Container className="mt-4">
						<Card className="text-center">
							<img src="https://coffeeatthree.com/wp-content/uploads/cold-brew-coffee-latte-7.jpg" className="img-fluid" alt="" />
						</Card>
					</Container>
				</Col>
				<Col xs={12} md={6}>
					<Container className="mt-4">
						<Card className="text-center">
							<Card.Body>
								<Card.Subtitle>
									<h4 className="mt-4">Description</h4>
								</Card.Subtitle>
								<Card.Text>{productInfo.description}
								</Card.Text>
								<Card.Subtitle>
									<h4 className="mt-5">Price</h4>
								</Card.Subtitle>
								<Card.Text>PHP {productInfo.price}
								</Card.Text>
							</Card.Body>
							{ 	user.id !== null ?
									<Button variant="warning" className="btn-block mb-0" onClick={buy}>Order</Button>
								:
									<Link className="btn btn-success btn-block mb-0" to="/login">Login to Order</Link>
							}
						</Card>
					</Container>
				</Col>
			</Container>
		</div>
	);
};