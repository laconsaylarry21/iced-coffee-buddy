
import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function AdminPanel () {
	const {user} = useContext(UserContext);

	return (
		!user.isAdmin
		?
			<Navigate to="/" replace={true}/>
		:
		<>
			<div className="outer-wrapper p-2">
				<div className="form-wrapper col-md-6 col-lg-4 text-white">
					<Container>
						<img src="admin-icon.png" alt="" className="my-2 centerIcon" />
						<h3 className="text-center mb-3">ADMIN PANEL</h3>
						<Container className="my-4">
							<h5 className="mb-1">Products</h5>
							<Link className="btn btn-success btn-block mb-2" to="/adminpanel/add-item">Add Item</Link>
							<Link className="btn btn-success btn-block mb-2" to="/adminpanel/allproducts">View All</Link>
						</Container>
						<Container className="my-4">
							<h5 className="mb-1">Users</h5>
							<Link className="btn btn-success btn-block mb-2" to="/login">View All</Link>
							<Link className="btn btn-success btn-block mb-2" to="/login">Update Role</Link>
						</Container>
					</Container>
				</div>
			</div>
		</>
	);
};