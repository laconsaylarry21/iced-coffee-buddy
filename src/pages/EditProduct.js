
import { Form, Button, Container} from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useState, useEffect } from 'react';


export default function UpdateProduct () {
	let token = localStorage.getItem('accessToken');

	const [productInfo, setProductInfo] = useState({
		name: null,
		description: null,
		price: null
	});

	const {id} = useParams()

	useEffect(() => {
		fetch(`https://sheltered-refuge-40596.herokuapp.com/products/${id}`).then(res => res.json()).then(convertedData => {
			// console.log(convertedData);

			setProductInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price,
				isActive: convertedData.isActive
			})
		});
	},[id])
	
	const [productName, setProductName] = useState('');
	const [productDescription, setProductDescription] = useState('');
	const [productPrice, setProductPrice] = useState('');

	// Archive Product
	const archiveProduct = async (eventSubmit) => {
		eventSubmit.preventDefault();
		const isProductArchived = await fetch(`https://sheltered-refuge-40596.herokuapp.com/products/${id}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			}
		}).then(response => response.json()).then(dataJSON => {
			console.log(dataJSON);
			if (dataJSON) {
				return true;
			} else {
				return false;
			}
		})
		console.log(isProductArchived);
		
		if (isProductArchived) {
			await Swal.fire({
				icon: "success",
				title: 'Success!',
				text: 'Product has been archived and will be currently unavailable for ordering.'
			})
			window.location.href = "/products";
		} else {
			await Swal.fire({
				icon: "error",
				title: 'Something Went Wrong',
				text: 'Try Again Later!'
			})
		}
	};

	// Unarchive Product
	const unarchiveProduct = async (eventSubmit) => {
		eventSubmit.preventDefault();
		const isProductUnarchived = await fetch(`https://sheltered-refuge-40596.herokuapp.com/products/${id}/unarchive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			}
		}).then(response => response.json()).then(dataJSON => {
			console.log(dataJSON);
			if (dataJSON) {
				return true;
			} else {
				return false;
			}
		})
		console.log(isProductUnarchived);
		
		if (isProductUnarchived) {
			await Swal.fire({
				icon: "success",
				title: 'Success!',
				text: 'Product has been unarchived and is now available for ordering.'
			})
			window.location.href = "/products";
		} else {
			await Swal.fire({
				icon: "error",
				title: 'Something Went Wrong',
				text: 'Try Again Later!'
			})
		}
	};

	// Update Product
	const updateProduct = async (eventSubmit) => {
		eventSubmit.preventDefault();

		const isProductUpdated = await fetch(`https://sheltered-refuge-40596.herokuapp.com/products/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
			    name: productName,
			    description: productDescription,
			    price: productPrice
			})
		}).then(response => response.json()).then(dataJSON => {
			console.log(dataJSON);
			if (dataJSON) {
				return true;
			} else {
				return false;
			}
		})
		console.log(isProductUpdated);
		
		if (isProductUpdated) {
			setProductName('');
			setProductDescription('');
			setProductPrice('');

			await Swal.fire({
					icon: "success",
						title: 'Product Update Successful!',
						text: 'Updated product details have been saved and should now reflect in the Products Catalog page.'
					})
			window.location.href = "/products";
		} else {
			await Swal.fire({
				icon: "error",
				title: 'Something Went Wrong',
				text: 'Try Again Later!'
			})
		}
	};
	
	return (
		<div>
			<div className="outer-wrapper p-2">
				<div className="form-wrapper col-md-6 col-lg-4">
					<Container className="text-white">
						<h3 className="mt-3 text-center">Update Product</h3>
						<Form onSubmit={e => updateProduct(e)}>
							<Form.Group>
								<Form.Label>Product Name:</Form.Label>
								<Form.Control type="text"
								required
								defaultValue={productInfo.name}
								onChange={event => {setProductName(event.target.value)}}
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label>Description:</Form.Label>
								<Form.Control type="text"
								required
								defaultValue={productInfo.description}
								onChange={event => {setProductDescription(event.target.value)}}
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label>Price:</Form.Label>
								<Form.Control type="number"
								required
								defaultValue={productInfo.price}
								onChange={event => {setProductPrice(event.target.value)}}
								/>
							</Form.Group>
							<Form.Group>
							<Button className="btn-primary btn-block mt-5" type="submit">Submit</Button>
							</Form.Group>
							{
								productInfo.isActive?
									<Button variant="warning" className="btn-block mb-3" onClick={archiveProduct}>Archive</Button>
								:
									<Button variant="warning" className="btn-block mb-3" onClick={unarchiveProduct}>Unarchive</Button>
							}
						</Form>
					</Container>
				</div>
			</div>
		</div>
	);
};