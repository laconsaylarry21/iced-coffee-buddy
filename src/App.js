import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Catalog from './pages/Products';
import ProductView from './pages/ProductView';
import AdminPanel from './pages/AdminPanel';
import Login from './pages/Login';
import Logout from './pages/Logout';
import AddItem from './pages/AddItem';
import AllProducts from './pages/AllProducts';
import EditProduct from './pages/EditProduct';
import ErrorPage from './pages/Error';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    });
  }

  useEffect(() => {
    // console.log(user);
    let token = localStorage.getItem('accessToken');
    // console.log(token);

    fetch('https://sheltered-refuge-40596.herokuapp.com/users/my-profile', {
      headers: {
          Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(convertedData => {
      // console.log(convertedData)
      if (typeof convertedData._id !== "undefined") {
        setUser({
          id: convertedData._id,
          isAdmin: convertedData.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null
        });
      }
    });
    
  },[user]);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
          <AppNavBar />
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/register' element={<Register />} />
            <Route path='/products' element={<Catalog />} />
            <Route path='/login' element={<Login />} />
            <Route path='/logout' element={<Logout />} />
            <Route path='/adminpanel' element={<AdminPanel />} />
            <Route path='/adminpanel/allproducts' element={<AllProducts />} />
            <Route path='/adminpanel/allproducts/modify/:id' element={<EditProduct />} />
            <Route path='/products/view/:id' element={<ProductView />} />
            <Route path='/adminpanel/add-item' element={<AddItem />} />
            <Route path='*' element={<ErrorPage />} />
          </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
