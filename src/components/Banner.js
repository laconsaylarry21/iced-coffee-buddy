import {Row, Col} from 'react-bootstrap';

export default function Banner({bannerData}) {
	return(
		<Row className="p-5 my-5">
			<Col>
				<h1 className="homeText pt-5 mb-3 mt-5"> {bannerData.title}</h1>
				<p className="homeTextSlogan mt-3 mb-4"> {bannerData.content}</p>
				<a className="btn homeBtnView px-4" href="/products">View Recipes</a>
			</Col>
		</Row>
	);
}