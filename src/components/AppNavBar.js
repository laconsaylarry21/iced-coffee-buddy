import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';

function AppNavBar() {
	const {user} = useContext(UserContext);
	return(
		<Navbar className="navBar px-lg-5" expand="lg">
				<img src="icedcoffee.gif" alt="" className="navBarIcon" />
				<Navbar.Brand className="text-white">Iced Coffee Buddy</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse>
					<Nav className="ml-auto">
						<Link to="/" className="nav-link text-white">Home</Link>
						<Link to="/products" className="nav-link text-white">Products</Link>
						{
							user.isAdmin ?
							<Link to="/adminpanel" className="nav-link text-white">Admin Panel</Link>
							:
							console.log(user.isAdmin)
						}
						{ 	user.id !== null ?
								<Link to="/logout" className="nav-link text-white">Logout</Link>
							:
							<>
								<Link to="/register" className="nav-link text-white">Register</Link>
								<Link to="/login" className="nav-link text-white">Login</Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>	
		</Navbar>
	);
};

export default AppNavBar;