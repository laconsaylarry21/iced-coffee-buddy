import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {
	return (
		
				<Card className="cardForm m-4 pt-2 px-4 pb-0 d-md-inline-flex d-sm-inline-flex mr-auto">				
					<Card.Body className="text-center">
						<Card.Title className="cardTitle">
							{productProp.name}
						</Card.Title>
						<Card.Text className="cardDesc">
							{productProp.description}
						</Card.Text>
						<Card.Text className="cardPrice">
							Php {productProp.price}
						</Card.Text>
						<Link to={`view/${productProp._id}`}class="btn viewBtn">
							View Product
						</Link>
					</Card.Body>
				</Card>
	)
}