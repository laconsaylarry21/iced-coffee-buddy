import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function AllProductCard({productProp}) {

	return (
				<Card className="cardForm m-4 pt-2 px-4 pb-0 d-md-inline-flex d-sm-inline-flex mr-auto">				
					<Card.Body className="text-center">
						<Card.Title className="cardTitle">
							{productProp.name}
						</Card.Title>
						<Card.Text className="cardDesc">
							{productProp.description}
						</Card.Text>
						<Card.Text className="cardPrice">
							Php {productProp.price}
						</Card.Text>
						<Card.Text>
							Available: {
								productProp.isActive?
									<span className="text-success">Yes</span>
								:
									<span className="text-danger">No</span>}
						</Card.Text>

						{
							productProp.isActive?
								<Link to={`modify/${productProp._id}`}class="btn-warning">
									<Button variant="warning" className="btn-block" >Update/Archive</Button>
								</Link>
							:
								<Link to={`modify/${productProp._id}`}class="btn-warning">
									<Button variant="warning" className="btn-block" >Update/Unarchive</Button>
								</Link>
						}
						
					</Card.Body>
				</Card>
	)
}